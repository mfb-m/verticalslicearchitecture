﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Domain.Entitites
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public IList<Task> Tasks { get; set; }
    }
}
