﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Domain.Entitites
{
    public class Task
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Date { get; set; }
    }
}
