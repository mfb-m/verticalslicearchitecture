﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDo.API.API.Shared;
using ToDo.Infrastructure.Bus.Commands;
using ToDo.Infrastructure.Bus.Commands.Queries;
using ToDo.Services.Modules.Task.GetTasksByUserId;

namespace ToDo.API.API.Controllers.V1
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class TasksController : TodoControllerBase
    {
        public TasksController(ICommandBus commandBus, IQueryBus queryBus) : base(commandBus, queryBus)
        {
        }

        // GET: api/Users
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetTasksByUserIdQuery getTasksByUserIdQuery)
        {
            var response = await queryBus.Send(getTasksByUserIdQuery);
            return Ok(response);
        }
    }
}