﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo.Infrastructure.Bus.Commands;
using ToDo.Infrastructure.Bus.Commands.Queries;

namespace ToDo.API.API.Shared
{
    public class TodoControllerBase : ControllerBase
    {
        protected ICommandBus commandBus;
        protected IQueryBus queryBus;

        public TodoControllerBase(ICommandBus commandBus, IQueryBus queryBus)
        {
            this.commandBus = commandBus;
            this.queryBus = queryBus;
        }
    }
}
