﻿using Bogus;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using ToDo.Domain.Entitites;
using ToDo.Infrastructure.EntityFramework;

namespace ToDo.Services.Shared
{
    public static class SeedExtensions
    {
        public static IWebHost Seed(this IWebHost webHost)
        {
            var scope = webHost.Services.CreateScope();
            var client = scope.ServiceProvider.GetService<ToDoDbContext>();
            var logger = scope.ServiceProvider.GetService<ILogger<ToDoDbContext>>();

            logger.LogInformation("Seed data start");

            var Tasks = new Faker<Task>()
                .RuleFor(o => o.Id, (f, e) => Guid.NewGuid())
                .RuleFor(o => o.Description, (f, e) => f.Lorem.Paragraph())
                .RuleFor(o => o.Date, (f, e) => f.Date.PastOffset())
                .Generate(1000);

            var users = new Faker<User>()
                .RuleFor(o => o.Id, (f, e) => Guid.NewGuid())
                .RuleFor(o => o.Name, (f, e) => f.Internet.UserName())
                .RuleFor(o => o.ImageUrl, (f, e) => f.Internet.Avatar())
                .RuleFor(o => o.Tasks, (f, e) => { return e.Tasks = new List<Task>(f.PickRandom(Tasks, 7)); } )
                .Generate(3);

            client.Users.AddRange(users);
            client.SaveChanges();

            logger.LogInformation("Seed data ended");
            return webHost;
        }

    }
}
