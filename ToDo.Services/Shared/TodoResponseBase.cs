﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Services.Shared
{
    public class TodoResponseBase<T>
    {
        public bool IsValid { get; set; }
        public T Payload { get; set; }
        public ICollection<string> Errors { get; set; }
        public int Count { get; set; }
        public int Total { get; set; }
    }
}
