﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDo.Infrastructure.Bus.Commands.Queries;
using ToDo.Services.Shared;

namespace ToDo.Services.Modules.Task.GetTasksByUserId
{
    public class GetTasksByUserIdQuery : IQuery<TodoResponseBase<IEnumerable<Domain.Entitites.Task>>>
    {
        public Guid UserId { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public Orderby OrderBy { get; set; }
        public OrderbyType OrderByType { get; set; }
    }

    public enum OrderbyType
    {
        Ascending,
        Descending
    }

    public enum Orderby
    {
        Date
    }
}
