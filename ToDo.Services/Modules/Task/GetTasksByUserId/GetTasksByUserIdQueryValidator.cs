﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Services.Modules.Task.GetTasksByUserId
{
    public class GetTasksByUserIdQueryValidator : AbstractValidator<GetTasksByUserIdQuery>
    {
        public GetTasksByUserIdQueryValidator()
        {
            RuleFor(o => o.UserId).NotEmpty().NotNull();
            RuleFor(o => o.PageNumber).NotEmpty().NotNull();
            RuleFor(o => o.PageSize).NotEmpty().NotNull();
        }
    }
}
