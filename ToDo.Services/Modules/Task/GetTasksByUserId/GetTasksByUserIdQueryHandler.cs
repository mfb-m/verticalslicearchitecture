﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDo.Infrastructure.Bus.Commands.Queries;
using ToDo.Infrastructure.EntityFramework;
using ToDo.Services.Shared;
using ToDo.Infrastructure.EntityFramework.Extensions;
using System.Linq.Dynamic.Core;

namespace ToDo.Services.Modules.Task.GetTasksByUserId
{
    public class GetTasksByUserIdQueryHandler : IQueryHandler<GetTasksByUserIdQuery, TodoResponseBase<IEnumerable<Domain.Entitites.Task>>>
    {
        private readonly ToDoDbContext _toDoDbContext;

        public GetTasksByUserIdQueryHandler(ToDoDbContext toDoDbContext)
        {
            _toDoDbContext = toDoDbContext;
        }

        public Task<TodoResponseBase<IEnumerable<Domain.Entitites.Task>>> Handle(GetTasksByUserIdQuery request, CancellationToken cancellationToken)
        {
            var total = _toDoDbContext.Tasks.Count();

            var result = _toDoDbContext.Tasks.AsQueryable()
                .OrderBy($"{request.OrderBy.ToString()} {request.OrderByType.ToString()?.ToLower()}")
                .GetPaged(request.PageNumber, request.PageSize);

            var response = new TodoResponseBase<IEnumerable<Domain.Entitites.Task>>
            {
                Count = result.Count(),
                Errors = null,
                IsValid = true,
                Payload = result,
                Total = total
            };
            return System.Threading.Tasks.Task.FromResult(response);
        }
    }
}
