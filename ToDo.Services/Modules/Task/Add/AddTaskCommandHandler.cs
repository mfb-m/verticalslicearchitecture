﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDo.Infrastructure.Bus.Commands;
using ToDo.Infrastructure.EntityFramework;

namespace ToDo.Services.Modules.Task.Add
{
    class AddTaskCommandHandler : ICommandHandler<AddTaskCommand>
    {
        private readonly ToDoDbContext _toDoDbContext;

        public AddTaskCommandHandler(ToDoDbContext toDoDbContext)
        {
            _toDoDbContext = toDoDbContext;
        }

        public Task<Unit> Handle(AddTaskCommand request, CancellationToken cancellationToken)
        {
            var task = new Domain.Entitites.Task
            {
                Date = DateTimeOffset.UtcNow,
                Description = request.Desription,
                Id = request.Id
            };
            _toDoDbContext.Tasks.Add(task);
            _toDoDbContext.SaveChanges();
            return System.Threading.Tasks.Task.FromResult(new Unit());
        }
    }
}
