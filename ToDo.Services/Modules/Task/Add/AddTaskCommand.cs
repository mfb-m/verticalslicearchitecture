﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDo.Infrastructure.Bus.Commands;

namespace ToDo.Services.Modules.Task.Add
{
    public class AddTaskCommand : ICommand
    {
        public Guid Id { get; set; }
        public string Desription { get; set; }
    }
}
