﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Services.Modules.Task.Add
{
    public class AddTaskCommandValidator : AbstractValidator<AddTaskCommand>
    {
        public AddTaskCommandValidator()
        {
            RuleFor(o => o.Id).NotNull().NotEmpty();
            RuleFor(o => o.Desription).NotNull().NotEmpty();
        }
    }
}
