﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Infrastructure.Bus.Commands
{
    public interface ICommand : IRequest { }
}
