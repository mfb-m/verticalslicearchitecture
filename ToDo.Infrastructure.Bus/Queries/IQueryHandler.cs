﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Infrastructure.Bus.Commands.Queries
{
    public interface IQueryHandler<in TQuery, TResponse> : IRequestHandler<TQuery, TResponse>
           where TQuery : IQuery<TResponse>
    {
    }
}
