﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Infrastructure.Bus.Commands.Queries
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}
